import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [

    { path: '', redirectTo: 'pokedex', pathMatch: 'full' },

    { path: 'pokedex', loadChildren: () => import('./components/poke-list/poke-list.module').then(m => m.PokeListModule) },
    { path: 'pokemon/:id', loadChildren: () => import('./components/poke-detail/poke-detail.module').then(m => m.PokeDetailModule) },
    { path: 'caught', loadChildren: () => import('./components/caught/caught.module').then(m => m.CaughtModule) },
    { path: 'wish', loadChildren: () => import('./components/wish/wish.module').then(m => m.WishModule) },

];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
