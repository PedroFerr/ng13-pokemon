export interface Pokedex{
    count: number;
    results: Pokemon[];
}

interface Pokemon {
    name: string;
    url: string;
}
