import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from 'src/environments/environment';
import { Pokedex } from './pokemon.interfaces';

@Injectable({
    providedIn: 'root'
})
export class PokemonService {

    _pokeURI = environment.pokeURI;

    constructor(private _http: HttpClient) { }

    getMaxOffset(max: number, offset: number): Observable<Pokedex> {
        return this._http.get<Pokedex>(`${this._pokeURI}pokemon?limit=${max}&offset=${offset}`);
    }

}
