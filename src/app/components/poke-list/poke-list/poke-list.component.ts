import { MediaMatcher } from '@angular/cdk/layout';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { identity, Observable, Subscription } from 'rxjs';

import { PokemonService } from '../../../services/pokemon.service';
import { Pokedex } from '../../../services/pokemon.interfaces';

import { PageEvent } from '@angular/material/paginator';

@Component({
    selector: 'app-poke-list',
    templateUrl: './poke-list.component.html',
    styleUrls: ['./poke-list.component.scss']
})
export class PokeListComponent implements OnInit, OnDestroy {

    isShow = false;
    imageLoader = true;

    mobileQuery: MediaQueryList;

    private _mobileQueryListener: () => void;


    pokemon$: Observable<Pokedex> | undefined;
    pokemon_subs: Subscription | undefined;

    length: number | undefined;
	pageSize = 8;
	pageIndex = 0;
	pageSizeOptions = [8, 12, 16];
    
    constructor(
        changeDetectorRef: ChangeDetectorRef, media: MediaMatcher,
        private api_pokedex: PokemonService
    ) {
        this.mobileQuery = media.matchMedia('(max-width: 600px)');
        this._mobileQueryListener = () => changeDetectorRef.detectChanges();
        this.mobileQuery.addListener(this._mobileQueryListener);
    }

    ngOnInit(): void {
        this.pokedex(this.pageSize, this.pageIndex);
    }

    ngOnDestroy(): void {
        this.mobileQuery.removeListener(this._mobileQueryListener);
        
        if (this.pokemon_subs) {
            this.pokemon_subs.unsubscribe();
        }
    }

    /**
     * Pokemon Index (max of "max") with offset of "offset"
     * 
     * @param max 
     * @param offset 
     */
    pokedex(max: number, offset: number) {

        this.pokemon$ = this.api_pokedex.getMaxOffset(max, offset);
        // we need the "length" (get "count", on API)
        this.pokemon_subs = this.pokemon$.subscribe(x => this.length = x.count);
    }

    /**
     * Handles the pagination, on each new page.
     * 
     * @param event - it is from the inside (it will make happen a new page)
     */
    handlePageEvent(event: PageEvent): void {
		// Refresh the global const:
		this.length = event.length;
		this.pageSize = event.pageSize;
        this.pageIndex = event.pageIndex;
		
		console.log(event)

        // this.pokedex(this.pageSize, this.pageIndex);
        this.pokedex(this.pageSize, this.pageIndex * this.pageSize);

	}


    toggleButton(id: string) {
        console.log(id);
        this.isShow = !this.isShow

        // this.manipulateValues('w', id);
     
        // const success = this.addWish(id);
        // if (success === null) {
        //     this.removeWish(id)
        // }
    }

    // addWish (id: any) {
    //     const wishlist = localStorage.getItem('w') as unknown as Array<any>
    //     const wishlistItem = wishlist.find((item: any) => item === id)
    //     console.log(wishlistItem)
    //     if (wishlistItem) {
    //         return
    //     }
    //     wishlist.push(id)
    //     localStorage.setItem('w', JSON.stringify(wishlist))
    // }

    // addWish(id: any) {
    //     // let's see if exists:
    //     const wishlist: any = localStorage.getItem('w') || [];

    //     // const itemFound = wishlist.includes(id);
    //     const foundItem = wishlist.find((item: any) => item.id === id);
    //     console.log(foundItem)

    //     if(foundItem) {
    //         // do nothing!
    //         return null;
    //     } else {
    //         wishlist.push({'id': id});
    //         localStorage.setItem('w', JSON.stringify(wishlist));
    //         return 1;
    //     }
    // }

    // removeWish(id: any) {

    // }

    // addNewItem (name: any, quantity: any, price: any) {


    //     // retrieve it (Or create a blank array if there isn't any info saved yet),
    //     const items = localStorage.getItem('w') || [];
      
      
    //     // add to it, only if it's empty
    //     const foundItem = items.filter((item: any) => item.name === name);
      
    //     if (item) {
    //       item.quantity += quantity;
    //     } else {
    //       items.push({
    //         name,
    //         quantity,
    //         price
    //       })
    //     }
      
    //     // then put it back.
    //     localStorage.setItem('w', JSON.stringify(items));
    //     console.log(items);
    //   }
      

    // manipulateValues(name: string, value: string) {

    //     // Get the existing data
    //     let existing = localStorage.getItem(name);

    //     // If no existing data, create an array
    //     // Otherwise, convert the localStorage string to an array
    //     const existing_1 = existing ? existing.split(',') : [];

    //     // Add new data to localStorage Array
    //     existing_1.push(value);

    //     // Save back to localStorage
    //     localStorage.setItem(name, existing_1.toString());

    // }

}
