import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokeListRoutingModule } from './poke-list-routing.module';

import { PokeListComponent } from './poke-list/poke-list.component';
import { AngularMaterialModule } from 'src/app/app-material.module';


@NgModule({
	imports: [
		CommonModule,
		AngularMaterialModule,
		PokeListRoutingModule,
	],
	declarations: [
        PokeListComponent
    ],
	providers: [],
})
export class PokeListModule { }
