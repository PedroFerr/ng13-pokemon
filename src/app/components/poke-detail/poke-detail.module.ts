import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PokeDetailRoutingModule } from './poke-detail-routing.module';

import { PokeDetailComponent } from './poke-detail/poke-detail.component';


@NgModule({
	imports: [
		CommonModule,
		PokeDetailRoutingModule,
	],
	declarations: [
        PokeDetailComponent
    ],
	providers: [],
})
export class PokeDetailModule { }
