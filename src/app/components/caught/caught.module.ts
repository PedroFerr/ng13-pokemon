import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CaughtRoutingModule } from './caught-routing.module';

import { CaughtComponent } from './caught/caught.component';


@NgModule({
	imports: [
		CommonModule,
		CaughtRoutingModule,
	],
	declarations: [
        CaughtComponent
    ],
	providers: [],
})
export class CaughtModule { }
