import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CaughtComponent } from './caught/caught.component';

const routes: Routes = [
    { path: '', component: CaughtComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CaughtRoutingModule { }
